package org.example;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }
    
    public static int sumar(int a, int b){
        return a+b;
    }

    public static int resta(int a, int b){
        return a-b;
    }

    public static int multiplicar(int a, int b){
        return a*b;
    }
}